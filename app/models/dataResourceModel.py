"""
MIT License

Copyright (c) 2024 Tecnalia, Basque Research & Technology Alliance (BRTA)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from __future__ import annotations

from typing import List

from pydantic import BaseModel, Field



class GxProducedBy(BaseModel):
    id: str = Field(..., serialization_alias='@id')


class GxExposedThrough(BaseModel):
    id: str = Field(..., serialization_alias='@id')


class CredentialSubjectDR(BaseModel):
    gx_name: str = Field(..., serialization_alias='gx:name')
    gx_description: str = Field(..., serialization_alias='gx:description')
    #gx_identifier: str = Field (..., serialization_alias='dcterms:identifier')
    type: str
    gx_containsPII: bool = Field(..., serialization_alias='gx:containsPII')
    gx_policy: str = Field(..., serialization_alias='gx:policy')
    gx_license: str = Field(..., serialization_alias='gx:license')
    gx_copyrightOwnedBy: str = Field(..., serialization_alias='gx:copyrightOwnedBy')
    gx_producedBy: GxProducedBy = Field(..., serialization_alias='gx:producedBy')
    gx_exposedThrough: GxExposedThrough = Field(..., serialization_alias='gx:exposedThrough')
    #dqv_hasQualityMeasurement:str=Field(...,serialization_alias='dqv:hasQualityMeasurement')
    id: str




class DataResourceModel(BaseModel):
    context: List[str] = Field(..., serialization_alias='@context')
    type: str
    id: str
    issuer: str
    validFrom: str
    credentialSubject: CredentialSubjectDR
