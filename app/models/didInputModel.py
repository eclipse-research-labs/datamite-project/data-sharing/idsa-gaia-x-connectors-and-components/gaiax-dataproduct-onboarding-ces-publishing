from __future__ import annotations

from typing import List

from pydantic import BaseModel


class ModelItem(BaseModel):

    certificateURL: str
    verificationMethod: str
    keyType:str
    algorithm:str


class DidInputModel(BaseModel):
    domain:str
    didList: List[ModelItem]