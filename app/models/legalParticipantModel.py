"""
MIT License

Copyright (c) 2024 Tecnalia, Basque Research & Technology Alliance (BRTA)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from __future__ import annotations

from typing import List, Optional


from pydantic import BaseModel, Field



class GxLegalRegistrationNumber(BaseModel):
    id: str = Field(...,serialization_alias='id')


class GxHeadquarterAddress(BaseModel):
    gx_countrySubdivisionCode: str = Field(..., serialization_alias='gx:countrySubdivisionCode')


class GxLegalAddress(BaseModel):
    gx_countrySubdivisionCode: str = Field(..., serialization_alias='gx:countrySubdivisionCode')


class CredentialSubjectLP(BaseModel):
    id: str
    type: str
    legalName: str = Field(..., serialization_alias='gx:legalName')
    gx_legalRegistrationNumber: GxLegalRegistrationNumber = Field(
        ..., serialization_alias='gx:legalRegistrationNumber'
    )
    gx_headquarterAddress: GxHeadquarterAddress = Field(
        ..., serialization_alias='gx:headquarterAddress'
    )
    gx_legalAddress: GxLegalAddress = Field(..., serialization_alias='gx:legalAddress')
    gx_terms_and_conditions_gaiaxTermsAndConditions: str = Field(
        ..., serialization_alias='gx-terms-and-conditions:gaiaxTermsAndConditions'
    )




class LegalParticipantModel(BaseModel):
    context: List[str] = Field(..., serialization_alias='@context')
    type: List[str]
    id: str
    issuer: str
    #issuanceDate: str
    validFrom:str
    credentialSubject: CredentialSubjectLP
