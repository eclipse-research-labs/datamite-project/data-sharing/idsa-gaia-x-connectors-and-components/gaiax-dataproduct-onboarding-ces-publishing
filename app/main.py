"""
MIT License

Copyright (c) 2024 Tecnalia, Basque Research & Technology Alliance (BRTA)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import logging
import os
from contextlib import asynccontextmanager

import uvicorn
from fastapi import FastAPI
from app.models.didInputModel import DidInputModel
from app.models.dataProductResourcesBody import DataProductResourcesModel
from app.models.legalParticipantBodyModel import LegalParticipantBodyModel
from app.utils.certificate_utils import include_proof_to_claim, sign_veriable_presentation
from app.utils.gaiax_utils import call_credentials_event_service, call_compliance_api
from app.utils.neo4j_utils import load_in_neo4j, get_data_products_list, get_data_products_by_name
from app.utils.postgres_utils import get_ces_data
from app.utils.selfDescription_utils import create_did_document, \
    create_legal_participant_verifiable_presentation, \
    create_data_product_with_resources_verifiable_presentation, \
    create_and_publish_data_product_with_resources_verifiable_presentation
from app.utils.logger_utils import send_export_gaiax_model_log, send_create_did_log,send_create_participant_log,send_publish_data_product_log, send_publish_data_product_log


from app.utils.logger_utils import generate_hash256, send_create_did_log

tags_metadata = [
    {
        "name": "Datamite External API"
    },
    {
        "name": "Datamite Internal API"
    },
    {
        "name": "Datamite testing API"
    }
]


logger = logging.getLogger("uvicorn.error")

DEBUG = os.getenv("DEBUG") or False

@asynccontextmanager
async def lifespan(app: FastAPI):
    logger = logging.getLogger("uvicorn.error")
    handler = logging.StreamHandler()
    handler.setFormatter(logging.Formatter("%(asctime)s | %(levelname)s | %(module)s | %(funcName)s | %(message)s"))
    logger.addHandler(handler)
    logging_level = logging.INFO
    if DEBUG:
        logging_level = logging.DEBUG
    logger.setLevel(logging_level)
    logger.propagate = False
    yield






app = FastAPI(lifespan=lifespan,title="DATAMITE", description=f"Datamite API documentation", openapi_tags=tags_metadata)



#######################################################################
# Create a did.json file
#Input: domain and certificate_URL: compplete chain for the certifcate
#Output: json document for the did
#######################################################################
@app.post("/diddocument", tags=["Datamite External API"])

async def create_did(body:DidInputModel):


    return create_did_document(body)





#######################################################################
# Create a Gaia-X Legal Participant
#Input: mandatory input data
#Output: JSON for LegalParticipant signed/unsigned
#######################################################################

@app.post("/legalparticipant", tags=["Datamite External API"])

async def createLegalParticipant(body:LegalParticipantBodyModel):

    return create_legal_participant_verifiable_presentation (body)




#######################################################################
# Create a definition for a DataProduct/Service Offering with resources DataResources
#Input: mandatory input data
#Output: JSON for DataProduct/Service Offering with DataResources
#######################################################################

@app.post("/dataproducts", tags=["Datamite Internal API"])

async def createDataProductWithResources(body:DataProductResourcesModel):

    return create_data_product_with_resources_verifiable_presentation(body)





#######################################################################
# Create a definition for a DataProduct/Service Offering with resources DataResources
#Input: mandatory input data
#Output: JSON for DataProduct/Service Offering with DataResources
#######################################################################


@app.post("/publish-dataproduct", tags=["Datamite External API"])

async def createAndPublishDataProduct(body:DataProductResourcesModel):

    return create_and_publish_data_product_with_resources_verifiable_presentation(body)



#######################################################################
# Create a definition for a DataProduct/Service Offering with resources DataResources
#Input: mandatory input data
#Output: JSON for DataProduct/Service Offering with DataResources
#######################################################################


@app.post("/publish-internal-dataproduct", tags=["Datamite External API"])

async def createAndPublishInternalDataProduct(data_product_uuid:str):

    return create_and_publish_data_product_with_resources_verifiable_presentation(body)






#######################################################################
# Method for calling to Gaia-X Compliance Service
#Input: a Veriable Presentation in json format
#Output: Compliance service output
#######################################################################


@app.post("/compliance", tags=["Datamite testing API"])
async def callToCompliance(body: dict):

    return call_compliance_api(body)




#######################################################################
# Method for calling to Credential Event Service and registers serviceOffering
#Input: a compliance Veriable Presentation for Service Offering
#Output: Credential Event Service output
#######################################################################
@app.post("/credential-event-service", tags=["Datamite testing API"])
async def callToCredentialsEventService(body: dict):

    return call_credentials_event_service(body)




#######################################################################
# Method for getting dataProducts from Neo4j

#Output: list of dataProducts
#######################################################################
@app.get("/dataproducts", tags=["Datamite External API"])

async def getDataProductsList():

    return get_data_products_list()



#######################################################################
# Method for getting dataProducts by name from Neo4j

#Output: dataProduct Name
#######################################################################
@app.get("/dataproducts/{dataProductName}", tags=["Datamite External API"])

async def getDataProductByName(dataProductName:str):

    return get_data_products_by_name(dataProductName)

#######################################################################
# Method for getting CesRecords

#Output: cesProducts
#######################################################################
@app.get("/cesrecords", tags=["Datamite External API"])

async def getCesRecords():

    return get_ces_data()

#######################################################################
# Method for export format from Dataproduct composition to Datamite External dataproducts

#Output: output for publishing data products
#######################################################################
@app.get("/export-dataproduct-composition-to-gaiax", tags=["Datamite External API"])

async def exportToGaiaXModel(unique_id:str):
    return export_to_dataproduct(unique_id)




#######################################################################
# Sign a verifiable credential
#Input: data for being signed and domain
#Output: data signed
#######################################################################
@app.post("/sign-vc", tags=["Datamite testing API"])

async def signVC(data: dict , issuerKey:str):

    return include_proof_to_claim(data, issuerKey)


#######################################################################
# Sign a verifiable presentation
#Input: data for being signed and domain
#Output: data signed
#######################################################################
@app.post("/sign-vp", tags=["Datamite testing API"])

async def signVP(data: dict , domain:str):


    return sign_veriable_presentation(data, domain)




#######################################################################
# Method for testing Neo4j
#Input: a compacted json file
#Output: json loaded in neo4j graph database
#######################################################################
@app.post("/loadNeo4j", tags=["Datamite testing API"])

async def loadNeo4j(dataProductName:str,data:dict):

    return load_in_neo4j(dataProductName, data)


@app.post("/hashtext", tags=["Datamite testing API"])

async def hash(data:dict):
    return send_create_did_log()



if __name__ == "__main__":

    uvicorn.run(app, host="0.0.0.0", port=9102)
