"""
MIT License

Copyright (c) 2024 Tecnalia, Basque Research & Technology Alliance (BRTA)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""





"""This module give the functionality to work with certificates.
"""
import logging
import os
import sys
from hashlib import sha1
from hashlib import sha256

import requests
from cryptography import x509
from cryptography.hazmat.primitives import serialization
from jwcrypto import jwk, jws
from jwcrypto.common import json_encode
from pyld import jsonld


logger = logging.getLogger("uvicorn.error")


def get_info_from_url(url):
    """
       Get information given a  URL

       :param url: url to be downloaded information.
       :return: complete text from the response.
       :rtype: json/text
    """

    logger.debug("requesting to specified url")
    try:
        response = requests.request("GET", url,verify=False)

        if response.status_code == 200:
            return response.text
        elif response.status_code == 404:
            return None
    except Exception as error:
        logger.debug("An error has happened getting information from url:"+str(error))



def generate_public_jwk(cert):

    """
       Generate Public JWK from a cert

       :param cert: X.509 certificate.
       :return: return a public key
       :rtype: str
    """

    logger.debug("init")

    try:

        publickey = jwk.JWK.from_pem(cert.public_key().public_bytes(
            encoding=serialization.Encoding.PEM, format=serialization.PublicFormat.SubjectPublicKeyInfo))
        logger.debug("publickey:%s" %publickey)
        return publickey
    except Exception as error:
        logger.debug("ERROR in generate public jwk method:" + str(error))
        return None


def locate_certificate(certificate):

    """
       Locate a certificate

       :param domain: domain of the certificate to be located.
       :return: cert
       :rtype: str
    """

    global raw_certs, cert_in_bytes
    logger.debug("getting information from the certificate")
    try:
        certificate = get_info_from_url(certificate)
        logger.debug("recovered information from the certificate")
        if certificate is not None:
            try:
                cert_in_bytes = bytes(certificate, 'UTF-8')
            except Exception as error:
                logger.debug("Error getting bytes from certificate %s" %error)
            try:
                raw_certs = x509.load_pem_x509_certificates(cert_in_bytes)
            except Exception as error:
                logger.debug("Error in x509.load_pem_x509_certificates %s" % error)
            cert = None
            for cert in raw_certs:
                try:
                    print (type(cert.subject))
                    cn = cert.subject.rfc4514_string()
                    cn_value = cn.split(',')[0].split('=')[1]
                    logger.debug(cert.subject)
                    logger.debug(cn)
                    logger.debug(cn_value)
                    if cn_value==domain:
                        break
                except Exception as error:
                    logger.debug("Error returning certificate %s" %error)

            return cert
        else:
            return None
    except Exception as error:
        logger.debug("An error has happened getting information from url:" + str(error))



def include_proof_to_claim(doc, issuerkey):

    """
       Giving an issuerKey and a json document sign it

       :param doc: document to be signed.
       :param issuerkey: issuerKey to be included in a signed document
       :return: return a signed document
       :rtype: str
    """

    global privkey, jwstoken
    try:
        logger.debug ("Signing doc: including proof to claim init")

        # Load PEM file
        try:
            with open(os.getenv("PRIVATE_KEY_FILE_NAME"), "rb") as pem_file:
                pem_data = pem_file.read()
                privkeyBytes = pem_data
        except Exception as error:
            logger.debug("Error reading private key:  %s" %error)
        try:
            logger.debug("Preparing private key in JWT format")
            privkey = jwk.JWK()
        except Exception as error:
            logger.debug("Error in private key generation: %s" %error)
        try:
            logger.debug("Import private key bytes")
            privkey.import_from_pem(privkeyBytes, password=None)
        except Exception as error:
            logger.debug("Error importing bytes to pem:: %s" %error  )
        normalized = None
        try:
            logger.debug("Normalizing doc in n-quads using URDNA2015 algorithm")
            normalized = jsonld.normalize(doc, {'algorithm': 'URDNA2015', 'format': 'application/n-quads'})
            logger.debug("Normalized doc in n-quads using URDNA2015 algorithm: %s" % normalized)
        except Exception as error:
            print("Error normalizing doc in n-quads:%s" %error)


        logger.debug("Hashing normalized document using sha256")

        hash_str = sha256(normalized.encode('utf-8'))
        logger.debug("Hashed normalized document using sha256: %s " %hash_str)

        try:
            jwstoken = jws.JWS(hash_str.hexdigest())
        except Exception as error:
            logger.debug("Error generating  jws.JWS %s" %jwstoken)

        logger.debug("Adding signature to jws token")
        jwstoken.add_signature(privkey, None,
                               json_encode({"alg": "PS256", "b64": False, "crit": ["b64"]}))
        logger.debug("Serializing jwstoken init")
        signed = jwstoken.serialize(compact=True)
        logger.debug("Serialized jwstoken end: %s" %signed)

        doc['proof'] = {
            "type": "JsonWebSignature2020",
            "proofPurpose": "assertionMethod",
            "verificationMethod": issuerkey,
            "jws": compact_token(signed)
        }
        logger.debug("Resulting doc= {doc}")
        logger.debug("Signing doc: including proof to claim end")
        return doc

    except Exception as error:
         print (error)
         sys.exit("Error during signing process: signClaim ")



def sign_veriable_presentation(verifiable_presentation: dict, domain: str):

    """
       Sign a Verifiable Presentation

       :param verifiable_presentation: verifiablePresentation to be signed.
       :param domain: issuerKey to be included in a signed document
       :return: return a signed document
       :rtype: str
    """



    didId = os.getenv("DID") + str(domain)

    logger.debug("data={verifiable_presentation.get('verifiableCredential')}")
    claims = []
    for vc in verifiable_presentation.get("verifiableCredential"):
        claims.append(include_proof_to_claim(vc, str(didId) + os.getenv("ISSUER_KEY")))
    del verifiable_presentation["verifiableCredential"]

    verifiable_presentation["verifiableCredential"] = claims
    logger.debug("end in signVariablePresentation method")
    return verifiable_presentation




def compact_token(token):

    """
       Compact token

       :param token: token to be compacted
       :return: return a compacted token

    """
    logger.debug("init")
    parts = token.split(".")
    logger.debug("end")
    return parts[0] + ".." + parts[2]




