"""
MIT License

Copyright (c) 2024 Tecnalia, Basque Research & Technology Alliance (BRTA)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""


"""This module give the functionality to work with Gaia-X self descriptions
"""

import hashlib
import json
import logging
import os
import re
import time
from datetime import datetime

import requests
import validators

from app.models.dataProductResourcesBody import DataProductResourcesModel, DataAccountItem, DataResourceInfo, \
    ServiceAccessPointInfo
from app.models.dataResourceModel import GxProducedBy, GxExposedThrough, CredentialSubjectDR, DataResourceModel
from app.models.legalParticipantBodyModel import LegalParticipantBodyModel
from app.models.legalParticipantModel import GxLegalRegistrationNumber, GxHeadquarterAddress, GxLegalAddress, \
    CredentialSubjectLP, LegalParticipantModel
from app.models.legalRegistrationModel import LR_VatIDModel, LR_LeicodeModel, LR_EORIModel
from app.models.serviceOfferingModel import GxTermsAndConditions, GxDataAccountExportItem, GxProvidedBy, \
    CredentialSubjectSO, ServiceOffering, GxAggregationOfItem, ServiceOfferingSAP, \
    CredentialSubjectSAP
from app.models.termAndCondModel import CredentialSubject, TermsAndConditionsModel
from app.utils.certificate_utils import get_info_from_url
from app.utils.certificate_utils import include_proof_to_claim
from app.utils.did_utils import generate_did
from app.utils.gaiax_utils import call_compliance, call_credentials_event_service
from app.utils.neo4j_utils import load_in_neo4j
from app.utils.postgres_utils import insert_data, check_data_product, insert_data_product
from app.utils.logger_utils import send_create_participant_log,send_publish_data_product_log
logger = logging.getLogger("uvicorn.error")



#Get the current datatime in ISO format
def get_current_date_time_in_iso():


    """
       Get the current data time in ISO format

       :return: current data time in iso format
       :rtype: str

    """

    logger.debug("getCurrentDateTimeInISO init")
    current_date = datetime.now()
    return current_date.isoformat()


#Check if given url for the participant is correct
def is_valid_jsond_url(url, domain):
    """
       Check if given url is valid

       :param url: url to be checked
       :return: true/false if the domain is valid or not
       :rtype: bool

    """
    if is_valid_url(url) and url.__contains__(domain) and url.endswith(".json"):
        return True
    else:
        return False



#Check if a domain is valid
def is_valid_domain(domain):

    """
       Check if the domain is valid

       :param domain: domain to be checked
       :return: true/false if the domain is valid or not
       :rtype: bool

    """


    logger.debug("isValidDomain init")
    # Regex to check valid
    # domain name.
    regex = "^((?!-)[A-Za-z0-9-]" + "{1,63}(?<!-)\\.)" + "+[A-Za-z]{2,6}"

    # Compile the ReGex
    p = re.compile(regex)

    # If the string is empty
    # return false
    if (domain == None):
        return False

    # Return if the string
    # matched the ReGex
    if (re.search(p, domain)):
        return True
    else:
        return False

#check is an URL is valid
def is_valid_url(url):

    """
       Check if the URL is valid

       :param url: url to be checked
       :return: true/false if the domain is valid or not
       :rtype: bool

    """

    logger.debug("isValidURL init")
    if url is not None:

        if validators.url(url) is not None:
            return True
        else:
            return False
    return True

#check is an URL is valid and resolvable
def is_complete_valid_url(url):
    """
       Check if the URL is complete

       :param url: url to be checked
       :return: true/false if the domain is valid or not
       :rtype: bool

    """

    logger.debug("isCompleteValidURL starts")
    if str is not None:

        if validators.url(url) and get_info_from_url(url) is not None:
            return True
        else:
            return False
    return True

#check if the ISOCode is correct
def check_iso_code(code):
    """
        Check if the isoCodel is valid

        :param code: isocode to be checked
        :return: true/false if the domain is valid or not
        :rtype: bool

     """

    logger.debug("checkISOCode starting")
    response= get_info_from_url(os.getenv("ISO_CODES_URL") + str(code))
    if response is None:
        return False
    else:
        return True


# Method for creating a Gaia-X Legal Participant
#it is formed by legalRegistrationNumber self-description + Terms and Conditions self-description and
# its legalParticipant self-description
def generate_complete_legal_person(body:LegalParticipantBodyModel, compliance:bool):


    """
        Generate a complete Legal Person self-description

        :param body: json format information as input
        :param compliance: boolean property true/false
        :return: JSON that presents a complete LegalPerson self-description
        :rtype: str

     """

    global docLRN
    logger.debug("generateCompleteLegalPerson SD")
    claims=[]
    if compliance==True and os.path.exists("privkey.pem") is None:
        return "ERROR: You should include a value for privateKey"
    else:

        # Generate Legal Registration Number
        logger.debug("Generate Legal Registration Number")
        try:
            docLRN = build_legal_registration_number_vc(body.domain, body.legalRegistrationType,
                                                                   body.legalRegistrationNumber)
            logger.debug("Generate Legal Registration Number={docLRN}")
        except Exception as error:
            logger.debug("Error generating legal registration number: %s" %error)
        try:
            if docLRN["type"]:
                #include legal Registration Number claim
                claims.append(docLRN)
                try:
                    # Generate Terms and Conditions
                    logger.debug("Generation TC SD")
                    docTC = build_terms_conditions_vc(body.domain, compliance)
                    logger.debug ("Terms and conditions self={docTC}")
                    #append self-description to be signed after if compliance==true
                    claims.append(docTC)
                except Exception as error:
                    logger.debug("Error generating terms and conditions self descriptor: %s" %error)
                #Generate Legal Person
                logger.debug("Generation LP SD")
                try:
                    docLP = build_legal_person_vc(body.domain, body.participantURL, body.legalName, body.headquarterAddress, body.legalAddress,
                                                    compliance)
                    # append self-description to be signed after if compliance==true
                    logger.debug ("Legal Participant self-desc ={docLP}")
                    claims.append(docLP)
                except Exception as error:
                    logger.debug("Error generating Legal Participant self descriptor: %s" %error)
                # Generate complete VP for Legal Person+ Term&Conditions+Legal Registration Number
                fileName=body.participantURL.replace(os.getenv("PROTOCOL") + str(body.domain) + str(os.getenv("FOLDER")),"")

                #save LegalParticipantVerifiablePresentation to file
                fileNamePath = os.getenv("GAIAX_SD_FOLDER") + fileName
                with open(fileNamePath, "w") as f:
                    json.dump(create_vp_participant(claims), f)

                #Call to Logging tool
                try:
                    send_create_participant_log(body.domain, body.participantURL,body.legalName)
                    logger.debug("Sending log about participant creation VP to Logger")
                except Exception as error:
                    logger.debug ("Error sending log about participant creation to Logger%s" %error)

                return create_vp_participant(claims)
            else:
                return docLRN
        except:
            return "Error generating Legal Person Verifiable Presentation"



def build_terms_conditions_vc(domain: str, compliance:bool):
    """
          Generate a terms and conditions verifiable credential in Json format

          :param domain: domain  as input
          :param compliance: boolean property true/false
          :return: JSON that presents a terms and conditions verifiable credential
          :rtype: str

       """

    global tc, claim
    logger.debug("build_terms_conditions_vc init")
    urlTermsAndConditions = os.getenv("PROTOCOL") + str(domain) + str(os.getenv("FOLDER")) + str(os.getenv("TC_URL_END"))

    termsAndConditions = os.getenv("TERMS_AND_CONDITIONS_TEXT")
    termsAndConditions = termsAndConditions.replace("\\n", "\n")

    didId = "did:web:"+str(domain)
    cxt = os.getenv("CONTEXT_TC")
    try:
        cs=CredentialSubject(context=cxt,type="gx:GaiaXTermsAndConditions",id=urlTermsAndConditions,gx_termsAndConditions=termsAndConditions)
    except Exception as error:
        logger.debug("Error at credential subject for terms and conditions: %s" %error)
    cxtList=get_context_list()

    try:
        tc = TermsAndConditionsModel(context=cxtList, type="VerifiableCredential", id=urlTermsAndConditions,
                                     issuer=didId,
                                     validFrom=get_current_date_time_in_iso(), credentialSubject=cs)
        logger.debug("terms and conditions self-desc={tc.model_dump(by_alias=True)}")


        logger.debug("build_terms_conditions_vc()::signClaim init")

    except Exception as error:
        logger.debug("Error at terms and conditions: %s" % error)

    fileNamePath = os.getenv("GAIAX_SD_FOLDER") + os.getenv("TC_URL_END")
    try:
        claim = sign_claim(compliance, fileNamePath, domain, tc.model_dump(by_alias=True))
        logger.debug(claim)
    except Exception as error:
        logger.debug("Error signing terms and conditions claim: %s" % error)
    return claim



def build_legal_person_vc(domain:str,participantURL:str,legName:str, headquarterAddress:str,legalAddress:str,compliance:bool):

    """
          Generate a legal person verifiable credential in Json format

          :param domain: domain  as input
          :param participantURL: participant complete URL
          :param legName: legal Name for the Legal Person
          :param headquarterAddress: headquarter Address
          :param legalAddress: legalAddress value
           :param compliance: boolean property true/false
          :return: JSON that presents a terms and conditions verifiable credential
          :rtype: str

       """

    global claim
    logger.debug("build_legal_person_vc init")

    urlTermsAndConditions = os.getenv("PROTOCOL") + str(domain) + str(os.getenv("FOLDER")) + str(
        os.getenv("TC_URL_END"))
    urlLRN = os.getenv("PROTOCOL") + str(domain) + str(os.getenv("FOLDER")) + str(os.getenv("RN_URL_END"))
    didId = "did:web:" + str(domain)

    cxtList=get_context_list()

    lrn=GxLegalRegistrationNumber(id=urlLRN)
    hqa=GxHeadquarterAddress(gx_countrySubdivisionCode=headquarterAddress)
    la=GxLegalAddress(gx_countrySubdivisionCode=legalAddress)



    try:
        cs = CredentialSubjectLP(id=participantURL, type="gx:LegalParticipant", legalName=legName,gx_legalRegistrationNumber=lrn.model_dump(mode='json'),gx_headquarterAddress=hqa.model_dump(mode='json'),gx_legalAddress=la.model_dump(mode='json'),gx_terms_and_conditions_gaiaxTermsAndConditions=urlTermsAndConditions)
    except Exception as error:
        logger.debug("Error at credential subject for legal person: %s" % error)


    cxtList = get_context_list()
    typeList=[]
    typeList.append("VerifiableCredential")

    try:
        lp = LegalParticipantModel(context=cxtList, type=typeList, id=participantURL, issuer=didId,
                                   validFrom=get_current_date_time_in_iso(), credentialSubject=cs)
        logger.debug(":legal person vc self-desc={lp.model_dump(by_alias=True)}")

    except Exception as error:
        logger.debug("Error generating legal participant model: %s" % error)

    fileNamePath = os.getenv("GAIAX_SD_FOLDER") + os.getenv("PARTICIPANT_END")

    try:
        claim = sign_claim(compliance, fileNamePath, domain, lp.model_dump(by_alias=True))
    except Exception as error:
        logger.debug("Error signing terms and conditions claim: %s" % error)

    return claim





def build_legal_registration_number_vc(domain:str, legalRegistrationType:str, legalRegistrationNumber: str):

    """
          Generate a legal registration number for production verifiable credential in Json format

          :param domain: domain  as input
          :param legalRegistrationType: legalRegistrationType parameter
          :param legalRegistrationNumber: legalRegistrationNumber parameter
          :return: JSON that presents a Legal registration number verifiable credential
          :rtype: str

       """

    global selfDescription
    logger.debug("build_legal_registration_number_vc init")
    #Generate complete URL for the LegalRegistrationNumber self-description
    urlLRN = os.getenv("PROTOCOL") + str(domain) + str(os.getenv("FOLDER")) + str(os.getenv("RN_URL_END"))


    #Preparate the input json for calling to Gaia-X NOTARIZATION_API
    contextList=[]
    contextList.append(os.getenv("CONTEXT_LRN"))
    if legalRegistrationType=='vatID':
        logger.debug("vatID")
        try:
            selfDescription=LR_VatIDModel(context=contextList,type="gx:legalRegistrationNumber",id=urlLRN,gx_vatID=legalRegistrationNumber)
        except Exception as error:
            logger.debug("Error generating vatID model: %s" % error)

    elif legalRegistrationType=='leiCode':
        logger.debug("leiCode")
        try:
            selfDescription=LR_LeicodeModel(context=contextList,type="gx:legalRegistrationNumber",id=urlLRN,gx_leiCode=legalRegistrationNumber)
        except Exception as error:
            logger.debug("Error generating leiCode model: %s" % error)

    else:
        logger.debug("EORI")
        try:
            selfDescription = LR_EORIModel(context=contextList, type="gx:legalRegistrationNumber", id=urlLRN,
                                          gx_eori=legalRegistrationNumber)
        except Exception as error:
            logger.debug("Error generating EORI model: %s" % error)
    #make request to NOTARIZATION_API

    try:

        logger.debug("request Notarization API")
        response = requests.request("POST", os.getenv("NOTARIZATION_API"), json=selfDescription.model_dump(by_alias=True))
        logger.debug("response Notarization API={response.status_code}")
        if response.status_code == 200:
            fileNamePath = os.getenv("GAIAX_SD_FOLDER") + os.getenv("RN_URL_END")
            with open(fileNamePath, "w") as f:
                json.dump(response.json(), f)
        #output from NOTARIZATION_API, JSON FILE WITH LEGAL REGISTRATION NUMBER SELF-DESCRIPTION
            return response.json()
        else:
            return "ERROR: Generating Legal Registration Number Verifiable credential"
    except Exception as error:
        logger.debug("Error calling to notarization API: %s" %error)







#generate DataAccoutExport items
def generate_data_account_export(data_account:list[DataAccountItem]):

    """
          Generate a DataAccount part for the DataProduct service offering

          :param data_account: data account information as input
          :return: JSON that presents a DataAccount information
          :rtype: str

       """


    logger.debug("generateDataAccountExport init")

    logger.debug (data_account)
    logger.debug (data_account.__len__())
    listDAE=[]
    for item in data_account:
        logger.debug(item)
        try:
            gx_data_account_exp=GxDataAccountExportItem(gx_requestType=item.requestType,gx_accessType=item.accessType,gx_formatType=item.formatType)
            listDAE.append(gx_data_account_exp)
        except Exception as error:
            logger.debug("Error generation Data Account Export Item %s: "%error)
    logger.debug("generateDataAccountExport end")

    return listDAE




def generate_dataresources_self_description(service_offering_id:str, data_resource_url:str, dataresource:DataResourceInfo, did_id:str, participant_url:str, data_resource_policy:str):

    """
          Generate a DataResouces self-description that is part for the DataProduct service offering

          :param service_offering_id: data product id
          :param data_resource_url: URL for the created dataresource URL
          :param dataresource: information for the dataresource
          :param did_id: decentralized identifier
          :param participant_url: url of the participant
          :param data_resource_policy: policy for the dataresource
          :return: JSON that presents a DataResource  information
          :rtype: str

       """

    global dr, cs
    logger.debug("generateDataresourcesSelfDescription init")
    """prodBy=GxProducedBy(id=participant_url)
    exposedT=GxExposedThrough(id=service_offering_id)"""
    logger.debug (dataresource)

    #quality=dataResourcePolicy

    try:
        cs=CredentialSubjectDR(gx_name=dataresource.name, gx_description=dataresource.description, type="gx:DataResource",
                           gx_containsPII=False, gx_policy=data_resource_policy, gx_license=dataresource.license, gx_copyrightOwnedBy=dataresource.copyrightOwnedBy,
                           gx_producedBy=GxProducedBy(id=participant_url), gx_exposedThrough=GxExposedThrough(id=service_offering_id), id=data_resource_url)

    except Exception as error:
        logger.debug("Error generatio credential subject for data resource %s" %error)


    try:
        cxtList=get_context_list()
        dr = DataResourceModel(context=cxtList, type="VerifiableCredential", id=data_resource_url, issuer=did_id,
                           validFrom=get_current_date_time_in_iso(),
                           credentialSubject=cs)

        logger.debug("data resouurce SD={dr.model_dump(by_alias=True)}")
        return dr
    except Exception as error:
        logger.debug("Error generation data resource model: %s" %error)




def generate_service_access_point_service(dataresource:ServiceAccessPointInfo, service_access_point_url:str, did_id:str, participant_url:str, service_policy:str, service_tc:str, data_account:list[DataAccountItem]):
    """
          Generate a ServiceAccessPoint service offering self-description that is part for the DataProduct service offering
          :param dataresource: information for the dataresource URL
          :param service_access_point_url: URL for the service access point URL
          :param did_id: decentralized identifier
          :param participant_url: url of the participant
          :param service_policy: policy for the service
          :param service_tc: service terms and conditions
          :param data_account: dataAccount information
          :return: JSON that presents a Service access point  information
          :rtype: str

       """


    logger.debug("generateServiceAccessPointService init")

    logger.debug(dataresource.openAPIURL)

    try:
        gxTC = GxTermsAndConditions(gx_URL=service_tc, gx_hash=encrypt_string(service_tc))
    except Exception as error:
        logger.debug("Error generating Terms and conditions model: %s" % error)

    listDAE = generate_data_account_export(data_account)
    logger.debug(listDAE)

    provided = GxProvidedBy(id=participant_url)
    #provided.model_dump(mode='json')
    #gxTC.model_dump(mode='json')
    try:
        cs = CredentialSubjectSAP(type="gx:ServiceOffering",
                              gx_providedBy=GxProvidedBy(id=participant_url) , gx_policy=service_policy,
                              gx_termsAndConditions=GxTermsAndConditions(gx_URL=service_tc, gx_hash=encrypt_string(service_tc)),
                              gx_openAPI=dataresource.openAPIURL,
                              gx_dataAccountExport=listDAE, id=service_access_point_url)
    except Exception as error:
        logger.debug("Error generating credential subjecto for service access point: %s" %error)
    try:
        cxtList = get_context_list()
        so = ServiceOfferingSAP(context=cxtList, type="VerifiableCredential", id=service_access_point_url, issuer=did_id,
                            validFrom=get_current_date_time_in_iso(), credentialSubject=cs)

        logger.debug("Service access point SD={so.model_dump(by_alias=True)}")
    except Exception as error:
        logger.debug("Error generating service access point model: %s" %error)
    return so



def generate_service_offering_with_resources(body: DataProductResourcesModel, compliance: bool):
    """
          Generate a DataProduct with resources
          :param body: complete body for creating a complete Data Product Self-description
          :param compliance: true/false if the self-description is going to be checked against compliance API
          :return: JSON that presents a Data Producut self description
          :rtype: str

       """

    global serviceAccessPointURL
    logger.debug("generate_service_offering_with_resources init")

    claims = []

    didId = "did:web:" + str(body.domain)

    curr_time = round(time.time() * 1000)
    # Get participant VP self-description to be included in the final VP
    logger.debug(f"Requesting particiant URL={body.participantURL}")

    # Get participant self-description information
    try:
        response = get_info_from_url(body.participantURL)

        if response is not None:
            participantSD = json.loads(response)

            # participantSD=requests.request("get",body.participantURL).json()

            # Generate url for the data product
            serviceOfferingId = os.getenv("PROTOCOL") + str(body.domain) + str(os.getenv("FOLDER")) + os.getenv(
                "DATAPRODUCT") + str("_") + str(curr_time) + str(
                os.getenv("FILE_END"))

            logger.debug(f"ServiceOffering Id={serviceOfferingId}")

            if len(body.dataResources) > 0:

                logger.debug("body.dataResources>0")
                logger.debug(f"body.dataresources={body.dataResources}")
                index = 0
                dataAggregation = []
                lstAggregation = []
                for dataresource in body.dataResources:
                    logger.debug("dataresource:")
                    logger.debug(dataresource)
                    # generate DataResource self-description and sign it

                    if dataresource.serviceAccessPointInfo is not None and dataresource.dataResourceInfo is not None:
                        logger.debug("DatasourceInfo and serviceAccessPoint is not None:")
                        # generate service Access Point Service Offering and sign it
                        if dataresource.serviceAccessPointInfo is not None:

                            logger.debug("Generate service access point self-description")
                            serviceAccessPointURL = os.getenv("PROTOCOL") + str(body.domain) + str(
                                os.getenv("FOLDER")) + os.getenv("SERVICE_ACCESS_POINT") + str(index) + str("_") + str(
                                curr_time) + str(

                                os.getenv("FILE_END"))
                            logger.debug("ServiceAccessPointURL={serviceAccessPointURL}")

                            try:
                                # generate a ServiceOffering similar to serviceAccessPoint
                                serviceAccessPointSD = generate_service_access_point_service(
                                    dataresource.serviceAccessPointInfo, serviceAccessPointURL, didId,
                                    body.participantURL, body.dataProductPolicy, body.dataProductTC, body.dataAccount)

                                logger.debug("ServiceAccessPointSD={serviceAccessPointSD.model_dump(by_alias=True)}")

                                datafileName = os.getenv("SERVICE_ACCESS_POINT") + str(index) + str("_") + str(
                                    curr_time) + str(".json")
                                fileNamePath = os.getenv("GAIAX_SD_FOLDER") + datafileName
                                try:
                                    claim = sign_claim(compliance, fileNamePath, body.domain,
                                                       serviceAccessPointSD.model_dump(by_alias=True))
                                    claims.append(claim)
                                except Exception as error:
                                    logger.debug("Error signing Service Access point claim: %s" % error)
                            except Exception as error:
                                logger.debug(
                                    "Error generating service access point service self-description: %s" % error)

                        # generate dataResource self-description
                        if dataresource.dataResourceInfo:

                            try:
                                logger.debug("Generate dataresource self-description")
                                dataresource_url = os.getenv("PROTOCOL") + str(body.domain) + str(
                                    os.getenv("FOLDER")) + os.getenv("DATARESOURCE") + str(index) + str("_") + str(
                                    curr_time) + str(os.getenv("FILE_END"))

                                logger.debug("DataresourceURL={dataresourceURL}")
                                dataAggregation.append(dataresource_url)
                                dataResourceSD = generate_dataresources_self_description(serviceAccessPointURL,
                                                                                         dataresource_url,
                                                                                         dataresource.dataResourceInfo,
                                                                                         didId, body.participantURL,
                                                                                         body.dataProductPolicy)

                                logger.debug("DataResourceSD={dataResourceSD.model_dump(by_alias=True)}")

                                datafileName = os.getenv("DATARESOURCE") + str(index) + str("_") + str(curr_time) + str(
                                    ".json")
                                fileNamePath = os.getenv("GAIAX_SD_FOLDER") + datafileName
                                try:
                                    claim = sign_claim(compliance, fileNamePath, body.domain,
                                                       dataResourceSD.model_dump(by_alias=True))
                                    claims.append(claim)
                                except Exception as error:
                                    logger.debug("Error signing Data Resource claim: %s" % error)
                            except Exception as error:
                                logger.debug("Error generating dataresource self-description: %s" % error)
                        index = index + 1

                # generate DataProduct including DataResource that it is included in dataAggregation list

                # gxTC = GxTermsAndConditions(gx_URL=body.dataProductTC, gx_hash=encrypt_string(body.dataProductTC))
                listDAE = generate_data_account_export(body.dataAccount)
                # logger.debug(listDAE)

                # provided = GxProvidedBy(id=body.participantURL)

                lstAggregation = []
                for data in dataAggregation:
                    # logger.debug (data)
                    aggregItem = GxAggregationOfItem(id=data)
                    lstAggregation.append(aggregItem)
                    # aggregation.append({"@id": data})

                try:
                    logger.debug("Generate service offering SD")
                    cxt_list = get_context_list()
                    cs = CredentialSubjectSO(gx_name=body.dataProductName, gx_description=body.dataProductDesc,
                                             type="gx:ServiceOffering",
                                             gx_providedBy=GxProvidedBy(id=body.participantURL),
                                             gx_policy=body.dataProductPolicy,
                                             gx_termsAndConditions=GxTermsAndConditions(gx_URL=body.dataProductTC,
                                                                                        gx_hash=encrypt_string(
                                                                                            body.dataProductTC)),
                                             gx_dataAccountExport=listDAE,
                                             id=serviceOfferingId, gx_aggregationOf=lstAggregation)

                    so = ServiceOffering(context=cxt_list, type="VerifiableCredential", id=serviceOfferingId,
                                         issuer=didId,
                                         validFrom=get_current_date_time_in_iso(), credentialSubject=cs)
                    logger.debug("ServiceOffering SD={so.model_dump(by_alias=True)}")
                    fileNamePath = os.getenv("GAIAX_SD_FOLDER") + os.getenv("DATAPRODUCT") + str("_") + str(
                        curr_time) + str(os.getenv("FILE_END"))
                    try:
                        claim = sign_claim(compliance, fileNamePath, body.domain, so.model_dump(by_alias=True))
                        claims.append(claim)
                    except Exception as error:
                        logger.debug("Error signing Service Offering claim: %s" % error)
                except Exception as error:
                    logger.debug("Error generating service offering self-descriptor: %s" % error)

                # push legalParticipant VP to DataProduct VP
                logger.debug(f"Pushing participant SD to other claims")
                sd = push_vc_to_participant(participantSD, claims)
                logger.debug(f"Pushed participant SD to other claims")

                fileNamePath = os.getenv("GAIAX_SD_FOLDER") + os.getenv("DATAPRODUCT") + str("_") + str(
                    curr_time) + str(os.getenv("FILE_END"))

                try:
                    send_publish_data_product_log(body.domain, body.participantURL, body.dataProductName, dataresource.serviceAccessPointInfo.openAPIURL)

                    logger.debug("Sending log about publishing data product to Logger")
                except Exception as error:
                    logger.debug(
                        "Error sending log about publishing data product to Logger%s" % error)








                # save the final DataProduct VP as a json file
                with open(fileNamePath, "w") as f:
                    json.dump(sd, f)

                return sd





            else:
                return "ERROR: No data resources information provided"
        else:
            return "ERROR: In participant getting information"
    except  Exception as error:
        logger.debug("Error getting participant url information %s " % error)



def create_vp_participant(claims: []):
    """
          Generate a Verifiable Presentation from a list of Verifiable Credentials
          :param claims: list of verifiable credentials
          :return: JSON that presents a complete Verifiable Presentation (VP) that includes "n" Verifiable credentials (VCs)
          :rtype: str

       """

    logger.debug("createVpParticipant init")

    veriCred = []
    for str in claims:

        veriCred.append(str)

    body = {
        "@context": "https://www.w3.org/ns/credentials/v2",
        "type": "VerifiablePresentation",
        "verifiableCredential":  veriCred
    }
    return body

def push_vc_to_participant(participantVC:dict, claims: []):

    """
          Push a Verifiable Credential
          :param participantVC: participant verifiable credential
          :param claims: list of verifiable credentials
          :return: JSON that presents a complete Verifiable Presentation (VP) that includes "n" Verifiable credentials (VCs)
          :rtype: str

       """
    print (type(participantVC))
    logger.debug("pushVcToParticipant init")
    #participant_jsonObject = json.loads(participantVC)

    for str in claims:
        try:
            print (str)
            participantVC["verifiableCredential"].append(str)
        except Exception as error:
            print ("Error pushing vc to vp %s: "%error)


    return participantVC





def encrypt_string(hash_string):
    """
          Encript a string using sha256 algorithm
          :param hash_string: string to be encrypted
          :return: encrypted string
          :rtype: str

    """


    logger.debug("encrypt_string init")
    sha_signature = hashlib.sha256(hash_string.encode()).hexdigest()
    return sha_signature


def get_context_list():
    """
          Generated a context list
          :return: an array of contexts
          :rtype: str

    """

    logger.debug("getContextList init")
    cxtList= [os.getenv("CONTEXT_ITEM1"), os.getenv("CONTEXT_ITEM2"), os.getenv("CONTEXT_ITEM3")]

    return cxtList



def create_did_document(domain:str):


    """
          Generated a did document
          :param domain: domain for the did document
          :return: did document in json format
          :rtype: str

    """


    logger.debug("createDidDocument init")

    try:
        if (domain) is None:  # The variable
            logger.debug('You must set value for domain')
    except NameError:
        logger.debug("This variable is not defined")
    else:
        if is_valid_domain(domain):

            try:
                logger.info("generareDid init")
                diddoc = generate_did(domain)
                logger.info("generareDid end")
                return diddoc
            except Exception as error:
                logger.debug("Error generating did document: %s " %error )
        else:
            return "Domain format is not correct"


def create_legal_participant_verifiable_presentation (body:LegalParticipantBodyModel):


    """
          Create a Legal participant verifiable presentation
          :param body: complete legal participant body model
          :return: Verifiable presentation for the Legal Participant
          :rtype: str

    """


    logger.debug("createLegalParticipantVerifiablePresentation init")

    try:
        if (body.domain or body.legalName or body.headquarterAddress or body.legalAddress or body.legalRegistrationType or body.legalRegistrationType or body.legalRegistrationNumber) is None:  # The variable
            logger.debug('You must set value for all input parameters')
    except NameError:
        logger.debug("This variables are not defined")
    else:
        #Generate Legal Person

        if is_valid_domain(body.domain) and is_valid_jsond_url(body.participantURL, body.domain):
            logger.debug("Domain anad participantURL are valid")
            if check_iso_code(body.legalAddress) and check_iso_code(body.headquarterAddress):
                if not (body.legalRegistrationType=='vatID' or body.legalRegistrationType=='leiCode' or body.legalRegistrationType=='EORI'):
                    logger.debug('Please introduce a valid value for legalRegistrationType (vatID, leiCode or EORI)')
                    return "Please introduce a valid value for legalRegistrationType (vatID, leiCode or EORI)"
                else:
                    try:
                        logger.debug("Generating CompleteLegalPerson")
                        selfDescription=generate_complete_legal_person(body, True)
                        logger.debug("Generated complete legal person")
                        return selfDescription
                    except:
                        return "ERROR: An error happen during legal participant generation self-description"

            else:
                return "ERROR: Please introduce a correct value for legalAddress or headquarterAddres, visit https://en.wikipedia.org/wiki/ISO_3166-2"

        else:
            return "ERROR: Parameter format error: please check Domain, participantURL or privateKeyURL input parameters"



def create_data_product_with_resources_verifiable_presentation(body):
    """
          Create a Data product with resources verifiable presentation
          :param body: complete legal participant body model
          :return: Verifiable presentation for the Data Product
          :rtype: str

    """

    if is_valid_domain(body.domain) and is_complete_valid_url(body.participantURL) and is_complete_valid_url(
            body.dataProductTC):
        try:
            # Check if dataproduct exists
            if not check_data_product(body.dataProductName):

                response = generate_service_offering_with_resources(body, True)
                # logger.debug (response)

                return response
            else:
                return "ERROR: There is a data product created with the same name"
        except:
            return "An error happen during service offering generation self-description"

    else:
        return "Parameter format error: please check Domain, participantURL or privateKeyURL input parameters"

def create_and_publish_data_product_with_resources_verifiable_presentation(body):
    """
          Create and publish in CES registry a Data product with resources verifiable presentation
          :param body: complete legal participant body model
          :return: Verifiable presentation for the Data Product
          :rtype: str

    """
    logger.debug(os.getenv("CES_CHECK"))
    if (os.getenv("CES_CHECK") == "False"):
        logger.debug("CES no activated")
        logger.debug("False")

    try:

        if body.domain =="":
            return ("Please introduce a value for domain")
        elif body.participantURL=="":
            return ("Please introduce a value for participant URL")

        elif is_valid_domain(body.domain) and is_complete_valid_url(body.participantURL) and is_complete_valid_url(body.dataProductTC):

            #Check if dataproduct exists
            if not check_data_product(body.dataProductName):

                response = generate_service_offering_with_resources(body, True)
                try:
                    resp = json.dumps(response)
                except:
                    logger.error("An error has happened during data product generation process")
                    return ("ERROR: An error has happened during data product generation process")
                try:
                    resCompl = call_compliance(json.loads(resp))
                    if not resCompl.startswith("ERROR"):
                        try:
                            #neo4jResult = load_in_neo4j(body.dataProductName, response)
                            neo4jResult=True
                        except Exception as error:
                            logger.error("An error during neo4j data product uploading process")
                            return ("ERROR: An error during neo4j data product uploading process:: %s" %error )
                        try:

                            logger.debug("Neo4jResult::")
                            logger.debug(neo4jResult)
                            logger.debug("CES Check::")
                            logger.debug(os.getenv("CES_CHECK"))
                            if neo4jResult:
                                if os.getenv("CES_CHECK") == "True":
                                    logger.debug("CES_CHECK is True")
                                    cesID = call_credentials_event_service(json.loads(resCompl))
                                    logger.debug("CESRESULT___________::")
                                    logger.debug(cesID)
                                    if not cesID.startswith("ERROR"):
                                        # cesID=CESresult.headers["location"]
                                        # insertData(cesID, response, resCompl)
                                        logger.debug("Inserting data starting")
                                        output=insert_data(cesID, response, resCompl)
                                        if not output.startswith("ERROR"):
                                            logger.debug(
                                            f"A data product has been created an published at CES with the following URL={cesID}")



                                            return "A data product has been created an published at CES with the following URL:: " + cesID
                                    else:
                                        logger.error("Data product has been generated correctly but CES service returns an error")
                                        return "ERROR: Data product has been generated correctly but CES service returns an error"
                                else:
                                    logger.debug ("Data product has been generated correctly but it has not beer registered in CES")
                                    return "Data product has been generated correctly but it has not been registered in CES"

                            else:
                                logger.error("Data product exists and it has not been registered in CES")
                                return "Data product exists and it has not been registered in CES"
                        except:
                            logger.error("CES service return an error")
                            return "ERROR: CES service return an error"
                    else:
                        logger.error("An error has happened during Compliance service call process")
                        return "ERROR: An error has happened during Compliance service call process"

                except:
                    logger.error("An error has happened during Compliance service call process")
                    return "ERROR: An error has happened during Compliance service call process"
                # loadInNeo4j
            else:
                return "ERROR: There is a data product created with the same name"




        else:
            logger.error("Parameter format error: please check Domain, participantURL or privateKeyURL input parameters")
            return ("ERROR: Parameter format error: please check Domain, participantURL or privateKeyURL input parameters")
    except Exception as error:
        logger.debug("An error has happened with the values that have been introduced")
        return "An error has happened with the values that have been introduced"



def sign_claim (compliance:bool, fileNamePath:str, domain:str, claim:dict):

    """
          sign claim
          :param compliance: true/false paramater
          :param fileNamePath: path to be saved the signed claim
          :param domain: domain used
          :param claim: claim to be signed
          :return: a signed claim
          :rtype: str

    """


    logger.debug("signClaim() init")
    if compliance == True:
        try:
            didId = os.getenv("DID") + str(domain)
            returned_claim = include_proof_to_claim(claim, str(didId) + os.getenv("ISSUER_KEY"))
            logger.debug(claim)

            with open(fileNamePath, "w") as f:
                json.dump(returned_claim, f)
        except Exception as error:
            logger.debug("Error including proof to claim: %s" %error)
    else:

        with open(fileNamePath, "w") as f:
            json.dump(claim, f)
    logger.debug("signClaim() end")
    return claim
