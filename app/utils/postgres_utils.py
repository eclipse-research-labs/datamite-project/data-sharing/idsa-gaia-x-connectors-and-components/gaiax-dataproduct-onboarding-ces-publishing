"""
MIT License

Copyright (c) 2024 Tecnalia, Basque Research & Technology Alliance (BRTA)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""


"""This module give the functionality to work with Postgres SQL Database
"""

import json
import logging
import os

import psycopg2 as psycopg2

logger = logging.getLogger("uvicorn.error")

def connect_postgres():
    """
       Connect to a Postgres database

       :return: return postgres connection

    """

    logger.debug("init")
    try:
        conn = psycopg2.connect(database=os.getenv("POSTGRES_DB"),
                                user=os.getenv("POSTGRES_USER"),
                                host=os.getenv("POSTGRES_HOST"),
                                password=os.getenv("POSTGRES_PASSWORD"),
                                port=os.getenv("POSTGRES_PORT"))
        logger.debug("end")
        return conn
    except Exception as error:
        logger.debug("Error connecting to Postgres database: " +str(error))


def insert_data(ces_id, data_product_data, ces_data):

    """
       Insert data in CES postgres table

       :param ces_id: cesID to be loaded
       :param data_product_data: json document describing data product
       :param ces_data: json document describing credential event service data
       :return: a new record into ces table if not exist

    """

    logger.debug("inserting data")
    conn = connect_postgres()

    #cur.execute("INSERT INTO ces(ces_id,ces_data VALUES("'cesID','cesData'")")
    #cur.execute("INSERT INTO ces(ces_id, ces_data VALUES (%s, %s)", [cesID], [cesData])
    try:
        cur = conn.cursor()
        sql = "INSERT INTO ces (ces_id, dataproduct_json,compliance_json) VALUES (%s, %s, %s)"
        logger.debug("insertData():sql={sql}")

        val = (ces_id, json.dumps(data_product_data), json.dumps(ces_data))
        cur.execute(sql, val)

        conn.commit()
        return "A new record has been inserted into ces"

    except:
        logger.error("ced_id exists")
        cur.close()
        return "ERROR: An error has happened during ces record insertion process"
        #conn.close()

def get_ces_data():

    """
       Get information from CES table

       :return: give a list of ces records
       :rtype : str

    """


    logger.debug("init")
    conn = connect_postgres()
    try:
        cur = conn.cursor()
        postgres_sql_select_query = "select * from ces"

        cur.execute(postgres_sql_select_query)
        logger.debug("Selecting rows from mobile table using cursor.fetchall")
        """ces_records = cur.fetchall()
        for row in ces_records:
            logger.debug("ces_id = ", row[0])
        """


        columns = [column[0] for column in cur.description]
        data = [dict(zip(columns, row)) for row in cur.fetchall()]
        logger.debug (data)
        return data
    except Exception as error:
        logger.error("ced_id Error getting ces data:"+str(error))
        cur.close()
        return "ERROR: An error has happened getting ces data"


def check_data_product(dataproduct_name:str):
    """
         Check dataproduct in dp postgres table

         :param dataproduct_name: dataProductName to be checked
         :return: True if data product exists

      """


    logger.debug("checkDataProduct")
    conn = connect_postgres()
    cur = conn.cursor()
    try:

        sql = "SELECT dataproductname FROM dp WHERE dataproductname= '" + dataproduct_name + "'"
        logger.debug("check data product name():sql={sql}")
        cur.execute(sql)
        records = cur.fetchall()
        if len(records) > 0:
            return True

    except Exception as error:
        logger.error("Error checking data product %s" %error)
        cur.close()
        return False
        #conn.close()



def insert_data_product(data_product_name:str):

    """
       Insert dataproduct in dp postgres table

       :param data_product_name: dataProductName to be loaded
       :return: a new record into dp table if not exist

    """



    logger.debug("inserting data")
    conn = connect_postgres()
    cur = conn.cursor()

    try:



        sql = "INSERT INTO public.dp (dataproductname) VALUES (%s)"

        logger.debug("insertData():sql={sql}")

        record_to_insert = (data_product_name,)
        cur.execute(sql, record_to_insert)



        conn.commit()
        count = cur.rowcount
        print(count, "Record inserted successfully")


    except Exception as error:
        print (error)
        logger.error("ced_id exists")
        cur.close()
        #conn.close()
