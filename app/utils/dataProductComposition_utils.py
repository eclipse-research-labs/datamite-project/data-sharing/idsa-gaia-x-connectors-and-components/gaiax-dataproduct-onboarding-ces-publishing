"""
MIT License

Copyright (c) 2024 Tecnalia, Basque Research & Technology Alliance (BRTA)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import os

"""This module give the functionality to work with Data Product Composition tool
"""
import json
import logging

import jsonschema
from jsonschema import validate

from app.models.dataProductResourcesBody import DataProductResourcesModel, DataAccountItem, ServiceAccessPointInfo, \
    DataResourceInfo, DataResource
from app.utils.certificate_utils import get_info_from_url
from app.utils.logger_utils import send_export_gaiax_model_log

logger = logging.getLogger("uvicorn.error")



def export_to_dataproduct(unique_id):
    """access_token=get_keycloak_access_token()
    print (access_token)"""

    response=get_info_from_url(os.getenv("DATAPRODUCT_COMPOSITION_URL")+str(unique_id))
    if response is not None:
        response.strip()
        response2=response.replace("\n","")

        data=json.loads(response2)
        print (data)
        print (data["results"][0])

        input_json = data["results"][0]
        print(input_json)

        # Nombre del fichero JSON
        filename = 'schema.json'

        # Abrir y cargar el contenido del fichero JSON
        with open("schema.json", "rb") as file:
            schema = json.load(file)
            
            print (data)
        try:
            validate(instance=input_json, schema=schema)
            print("Successfull validation")
            participant_url=""
            str_format = input_json["distribution"][0]["format"]
            domain=input_json["domain"][0]
            data_account = DataAccountItem(requestType="API", accessType="digital", formatType=str_format)
            listDAE = [data_account]

            listDataResource = []
            apiURL = input_json["distribution"][0]["accessURL"]
            sap = ServiceAccessPointInfo(openAPIURL=apiURL)
            name = input_json["title"]
            drinfo = DataResourceInfo(name=name, description=input_json["description"],
                                      license=input_json["license"]["accessURL"],
                                      copyrightOwnedBy=input_json["publisher"])

            dr = DataResource(serviceAccessPointInfo=sap, dataResourceInfo=drinfo)
            listDataResource.append(dr)

            print(input_json["termsAndConditions"])
            terms = input_json["termsAndConditions"]
            print(terms["URL"])
            policy = input_json["policy"]
            print(policy)
            print(len(policy))
            if len(policy) == 0:
                strPolicy = ""
            else:
                strPolicy = policy[0]
            dp = DataProductResourcesModel(
                participantURL=participant_url,
                domain=domain, dataProductName=name,
                dataProductDesc=input_json["description"], dataProductTC=terms["URL"], dataProductPolicy=strPolicy,
                dataAccount=listDAE, dataResources=listDataResource)

            try:



                send_export_gaiax_model_log(unique_id)
                logger.debug("Sending log about exporting internal model to external to Logger")
            except Exception as error:
                logger.debug(
                    "Error sending log about exporting internal model to external to Logger%s" % error)



            return (dp.model_dump(by_alias=True))
        except jsonschema.exceptions.ValidationError as err:
            print(f"Unsuccessfull validation: {err.message}")
            return "Unsuccessfull schema validation"

        

        
    else: return "No information with this uniqueid"

