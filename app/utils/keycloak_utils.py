"""
MIT License

Copyright (c) 2024 Tecnalia, Basque Research & Technology Alliance (BRTA)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""


"""This module give the functionality to work with neo4j.
"""

import logging
import os

import requests

logger = logging.getLogger("uvicorn.error")

def get_keycloak_access_token():


    # Define the URL for the token endpoint

    keycloak_url =os.getenv("KEYCLOAK_HOST_URL")


    # client ID and client secret
    keycloak_client_id =os.getenv("KEYCLOAK_CLIENT_ID")
    keycloak_client_secret = os.getenv("KEYCLOAK_CLIENT_SECRET")
    keycloak_username = os.getenv("KEYCLOAK_USER_NAME")
    keycloak_password = os.getenv("KEYCLOAK_PASSWORD")

    #headers
    headers = {'content-type':"application/x-www-form-urlencoded" }

    # Define the payload
    payload = {
        'grant_type': os.getenv("KEYCLOAK_GRANT_TYPE"),
        'client_id': keycloak_client_id,
        'client_secret': keycloak_client_secret,
        'username': keycloak_username,
        'password': keycloak_password

    }

    # Send the POST request
    try:
        response = requests.post(keycloak_url, data=payload, headers=headers)
        if response.status_code == 200:
            token_data = response.json()
            access_token = token_data.get('access_token')
            print("Access Token: {access_token}")
            return access_token
        else:
            print("Failed to obtain access token: {response.status_code}")
            print(response.text)
    except Exception as error:
        logger.debug("Error getting token from Keycloak: " +str(error))
    # Check the response

