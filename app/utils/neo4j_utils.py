"""
MIT License

Copyright (c) 2024 Tecnalia, Basque Research & Technology Alliance (BRTA)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""


"""This module give the functionality to work with neo4j.
"""


import json
import logging
import os
import time

from fastapi.responses import ORJSONResponse
from neo4j import GraphDatabase
from pyld import jsonld

logger = logging.getLogger("uvicorn.error")

def get_driver():

    """
       Get driver of the Neo4j database
       :return: return driver

    """

    driver = None
    logger.debug("init")
    try:

        driver=GraphDatabase.driver(os.getenv("NEO4J_URI"), auth=(os.getenv("NEO4J_USER"),os.getenv("NEO4J_PASSWORD")))
    except Exception as error:
        logger.debug("Error connecting to Neo4j database %s" %error)
    logger.debug("end")
    return driver




def load_in_neo4j(dataproduct_name:str, data:str):

    """
       Load a document to Neo4j database

       :param dataproduct_name: name of the data product
       :param data: document to be loaded
       :return: return result of complinace service
       :rtype: str
    """


    logger.debug("init")
    logger.debug("checkIfDataProductExists")
    result=check_if_data_product_exists(dataproduct_name)
    logger.debug('checkIfDataProductExists={result}')


    if not result:

        logger.debug("transform_jsonld_to_compact_json")
        sd_compact = transform_jsonld_to_compact_json(data)
        logger.debug("transform_jsonld_to_compact_json={sd_compact}")

        curr_time = round(time.time() * 1000)
        fileNamePath = os.getenv("NEO4J_IMPORT_FOLDER") + os.getenv("DATAPRODUCT_COMPACT")+str(curr_time)+str(".json")

        with open(fileNamePath, "w") as f:
            json.dump(sd_compact, f)

        driver = get_driver()
        dataset_path=os.getenv("NEO4J_IMPORT_FOLDER")+ os.getenv("DATAPRODUCT_COMPACT")+str(curr_time)+str(".json")
        logger.debug("dataset_path::={dataset_path}")

        #check if the dataproduct with the same name is in the neo4j catalog

        logger.debug("load_data_into_db()::")
        data=load_data_into_db(driver, dataset_path)
        logger.debug("load_data_into_db()={data}")
        #return Response(content=data, headers=headers)
        return data
    else:
        return False


def transform_jsonld_to_compact_json(json_content):


    """
       Transform a json in a compacted way

       :param json_content: json document to be compacted
       :return: a compacted json document
       :rtype: str
    """

    logger.debug("transform_jsonld_to_compact_json():init")

    context = {}

    compacted = jsonld.compact(json_content, context)
    logger.debug("transform_jsonld_to_compact_json():compacted json={compacted}")
    return compacted





def load_data_into_db(driver, dataset_path):
    """
         Load data  given a path


         :param driver: Neo4j database driver
         :param dataset_path: complete path to be loaded         :return:  document
         :rtype: str
      """

    logger.debug("load_data_into_db():init")

    # Empty DB
    query = """
        MATCH (n) DETACH DELETE n         
    
    summary = driver.execute_query(query,result_transformer_=neo4j.Result.consume)
    """
    # This query is only required to execute once in the DB
    logger.debug("load_data_into_db():create constraint")
    query ='CREATE CONSTRAINT n10s_unique_uri FOR (r:Resource) REQUIRE r.uri IS UNIQUE'
    
    try :
        aa, pp, kk = driver.execute_query(
            query
        )
    except Exception:
        pass

    # Initialize DB
    logger.debug("load_data_into_db():handleVocabUris")
    query = "CALL n10s.graphconfig.init({ handleVocabUris: 'MAP'})"
    try:
        aa, pp, kk = driver.execute_query(
            query
        )
    except Exception:
        pass

    # Load dataset into DB
    logger.debug("load_data_into_db():import DP in neo4j")
    query = 'CALL n10s.rdf.import.fetch("file://' + dataset_path + '", "JSON-LD")'
    # TODO: Borrar esta query. Es para pruebas en local
    #query = """
    #        CALL n10s.rdf.import.fetch("file:///var/lib/neo4j/import/Json_FRBRT_91838_2020_08_91838_02_uris_fechas_duplicates1700205742783.json",
    #        "JSON-LD")
    #    """

    logger.debug("load_data_into_db():Query import dataset={query}")
    try:
        records, summary, keys = driver.execute_query(
            query
        )
        logger.debug("records imported:={records}")
        logger.debug("length records imported={len(records)}")
        logger.debug (len(records))
        for record in records:
            logger.debug (record)
            logger.debug (record.get("triplesLoaded"))

            if record.get("triplesLoaded")==0:
                logger.debug("No data loaded")
                return "No data loaded into Neo4h graph database"
                break
            else:
                logger.debug("New data loaded")
                return True
    except:
        logger.debug ("No data loaded")
        #return "An error has been detected during neo4j json document upload process"
        return False

def check_if_data_product_exists(dataproduct_name:str):


    """
         Check if data products exists

         :param dataProductName: dataproduct name

         :return: True/False
         :rtype: bool
      """


    logger.debug("init")
    driver = get_driver()
    # query = 'MATCH (:ServiceOffering)-[:credentialSubject]->(n2), (n2)-[:aggregationOf]->(n3),(n2)-[:dataAccountExport]->(n4), (n3)-[:exposedThrough]-> (n5) where n2.`serviceOffering:name`=~(?i).*'+name +' return n2.`serviceOffering:name`, n2.`serviceOffering:description`,n2.policy,n5.protocol,n5.host,n5.openAPI, n3.name, n3.description,n3.license,n3.copyrightOwnedBy, n4.accessType, n4.requestType,n4.formatType'
    query = 'MATCH (:ServiceOffering)-[:credentialSubject]->(n2), (n2)-[:aggregationOf]->(n3),(n2)-[:dataAccountExport]->(n4), (n3)-[:exposedThrough]-> (n5) where n2.`serviceOffering:name`=' + "'" + dataproduct_name + "'" + ' return  n2.uri as serviceOfferingId, n2.`serviceOffering:name` as dataproductName, n2.`serviceOffering:description` as dataProductDescription,n2.policy as policy,n5.protocol as protocol,n5.host as host,n5.openAPI as openAPI, n3.name as dataResourceName, n3.description as dataResourceDescription,n3.license  as dataResourceLicense,n3.copyrightOwnedBy as dataResourceCopyrightOwnedBy, n4.accessType as dataProductAccessType, n4.requestType as dataProductRequestType,n4.formatType as dataProductFormatType'
    logger.debug(query)

    try:

        records_df = driver.execute_query(
            query,
            result_transformer_=lambda res: res.to_df(True)
        )


        if len(records_df)>0:
            logger.debug("Data product exists")
            return True
        else:

            return False
    except Exception as error:
        logger.debug ("Error: check if data product exist: "+str(error))

def get_data_products_by_name(dataproduct_name:str):

    """
         Get dataproduct information

         :param dataProductName: dataproduct name

         :return: dataproduct information
         :rtype: str
      """

    logger.debug("init")
    driver=get_driver()
    try:

        query = 'MATCH (:ServiceOffering)-[:credentialSubject]->(n2), (n2)-[:aggregationOf]->(n3),(n2)-[:dataAccountExport]->(n4), (n3)-[:exposedThrough]-> (n5) where n2.`serviceOffering:name`=' + "'" + dataproduct_name + "'" + ' return  n2.uri as serviceOfferingId, n2.`serviceOffering:name` as dataproductName, n2.`serviceOffering:description` as dataProductDescription,n2.policy as policy,n5.openAPI as openAPI, n3.name as dataResourceName, n3.description as dataResourceDescription,n3.license  as dataResourceLicense,n3.copyrightOwnedBy as dataResourceCopyrightOwnedBy, n4.accessType as dataProductAccessType, n4.requestType as dataProductRequestType,n4.formatType as dataProductFormatType'
        logger.debug (query)


        records_df = driver.execute_query(
            query,
             result_transformer_=lambda res: res.to_df(True)
        )

        """logger.debug(type(records_df))
        logger.debug (records_df)
        logger.debug (records_df.keys)"""

        # create a dictionary of replacements

        logger.debug("Length of records loaded={len(records_df)}")


        # logger.debug the DataFrame
        logger.debug(records_df)



        return ORJSONResponse(json.loads(records_df.to_json(orient="records")))
    except Exception as error:
        logger.debug("Error getting data product by name: "+ str(error))

def get_data_products_list():


    """
         Get dataproducts information



         :return: list of dataproducts information
         :rtype: array of json
      """

    logger.debug("init")
    driver = get_driver()
    try:
        #query = 'MATCH (:ServiceOffering)-[:credentialSubject]->(n2), (n2)-[:aggregationOf]->(n3),(n2)-[:dataAccountExport]->(n4), (n3)-[:exposedThrough]-> (n5) return n2.uri as serviceOfferingId, n2.`serviceOffering:name` as dataproductName, n2.`serviceOffering:description` as dataProductDescription,n2.policy as policy,n5.protocol as protocol,n5.host as host,n5.openAPI as openAPI, n3.name as dataResourceName, n3.description as dataResourceDescription,n3.license as dataResourceLicense,n3.copyrightOwnedBy as dataResourceCopyrightOwnedBy, n4.accessType as dataProductAccessType, n4.requestType as dataProductRequestType,n4.formatType as dataProductFormatType'
        query = 'MATCH (:ServiceOffering)-[:credentialSubject]->(n2), (n2)-[:aggregationOf]->(n3),(n2)-[:dataAccountExport]->(n4), (n3)-[:exposedThrough]-> (n5) return n2.uri as serviceOfferingId, n2.`serviceOffering:name` as dataproductName, n2.`serviceOffering:description` as dataProductDescription,n2.policy as policy,n5.openAPI as openAPI, n3.name as dataResourceName, n3.description as dataResourceDescription,n3.license as dataResourceLicense,n3.copyrightOwnedBy as dataResourceCopyrightOwnedBy, n4.accessType as dataProductAccessType, n4.requestType as dataProductRequestType,n4.formatType as dataProductFormatType'
        records_df = driver.execute_query(
            query,
            # result_transformer_=neo4j.Result.to_df
            result_transformer_=lambda res: res.to_df(True)
        )


        """with open("neooutput.json", "w") as f:
            json.dump(json.loads(records_df.to_json(orient="records")), f)
        """

        return ORJSONResponse(json.loads(records_df.to_json(orient="records")))
    except Exception as error:
        logger.debug("Error getting data products list: "+str(error))