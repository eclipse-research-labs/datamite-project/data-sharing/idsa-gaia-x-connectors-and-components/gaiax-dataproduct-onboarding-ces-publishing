"""
MIT License

Copyright (c) 2024 Tecnalia, Basque Research & Technology Alliance (BRTA)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""



"""This module give the functionality to work with gaia-x documents.
"""

from datetime import timezone
import json
import logging
import os

import requests

logger = logging.getLogger("uvicorn.error")



def call_compliance(body: dict):



    """
       Call to gaia-X compliance service internally

       :param body: verifiable credential/presentation to be checked
       :return: return result of complinace service
       :rtype: str
    """

    logger.debug("init")

    if body is not None:
        try:
            logger.debug("Call to compliance api")
            response = requests.request("POST", os.getenv("COMPLIANCE_API"), json=body)

            logger.debug("Compliance response::%s" % response.text)
            if response.status_code == 201:
                logger.debug("response.status_code==201")
                logger.debug("end")
                return response.text
            else:
                return "ERROR: Compliance Service returns an error: "+ response.text
        except Exception as error:
            return "ERROR: Compliance Service returns an error %s" %error
    else:
        return "ERROR: Please introduce a valid Verifiable Presentation"




def call_compliance_api(body: dict):


    """
       Call to gaia-X compliance API from external API

       :param body: verifiable credential/presentation to be checked
       :return: return result of complinace service
       :rtype: str
    """

    global response
    logger.debug("init")
    if body is not None:
        try:
            response = requests.request("POST", os.getenv("COMPLIANCE_API"), json=body)
            logger.debug("response::%s" % response.text)
        except Exception as error:
            logger.debug("Error calling to compliance service ::%s" %error)
        if response.status_code == 201:
            logger.debug("response.status_code==201")
            with open("complianceResult.json", "w") as f:
                json.dump(response.text, f)
            return json.loads(response.text)
        else:
            return json.loads(response.text)
    else:
        return "Please introduce a valid Verifiable Presentation"


def call_credentials_event_service(body: dict):
    """
       Call to gaia-X credential event service

       :param body: verifiable compliance presentation
       :return: return result of credential event service
       :rtype: json
    """

    global response
    logger.debug("init")
    if body is not None:

        from datetime import datetime
        dt = datetime.now(timezone.utc).replace(tzinfo=None)
        dt=dt.strftime('%Y-%m-%dT%H:%M:%SZ')


        logger.debug("Preparing self-description")

        sd = {
            "specversion": "1.0",
            "type": "eu.gaia-x.credential",
            "source": "/app",
            "time": dt,
            "datacontenttype": "application/json",
            "data": body
        }

        logger.debug("SD generated::%s " % sd)
        try:
            response = requests.request("POST", os.getenv("CREDENTIALS_EVENT_SERVICE_API"), json=sd)
        except Exception as error:
            logger.debug ("Error calling to credential event service:"+ str(error))

        logger.debug("Response status code::%s" % response.status_code)

        if response.status_code == 201:
            logger.debug("Preparing self-description:: status_code==201")
            logger.debug("status_code==201::%s" % response.headers)
            with open("credentialEventServiceResult.json", "w") as f:
                json.dump(str(response.headers), f)
            # logger.debug(response.headers["location"])

            return str(response.headers["location"])

        else:

            return "ERROR: CES service doesn't work perfectly"
    else:
        return "ERROR: CES body is empty"






"""
def callCredentialsEventService(body: dict):

  Call to gaia-X credential event service

       :param body: verifiable compliance presentation
       :return: return result of credential event service
       :rtype: json



    logger.debug("init")

    if body is not None:

        import datetime
        dt = datetime.datetime.utcnow().isoformat() + "Z"

        logger.debug("Preparing self-description")
        random_uuid = uuid.uuid4()
        uuid_str = str(random_uuid)

        jwt_token = jwt.encode(body,os.getenv("PRIVATE_KEY"), algorithm='RS256')

        sd = {
            "specversion": "1.0",
            "type": "eu.gaia-x.credential",
            "source": "/app",
            "subject": "",
            "id": uuid_str,
            "time": dt,
            "datacontenttype": "application/json",
            "data_base64": jwt_token
        }

        logger.debug("SD generated::%s " % sd)

        response = requests.request("POST", os.getenv("CREDENTIALS_EVENT_SERVICE_API"), json=sd)

        logger.debug("Response status code::%s" % response.status_code)

        if response.status_code == 201:
            logger.debug("Preparing self-description:: status_code==201")
            logger.debug("status_code==201::%s" % response.headers)
            with open("credentialEventServiceResult.json", "w") as f:
                json.dump(str(response.headers), f)
            # logger.debug(response.headers["location"])

            return response.headers["location"]

        else:

            return None
"""