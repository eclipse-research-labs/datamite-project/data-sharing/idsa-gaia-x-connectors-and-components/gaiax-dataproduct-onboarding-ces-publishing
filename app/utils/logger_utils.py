"""
MIT License

Copyright (c) 2024 Tecnalia, Basque Research & Technology Alliance (BRTA)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""


"""This module give the functionality to work with neo4j.
"""
import logging
import os
from datetime import datetime
import requests
from app.utils.keycloak_utils import get_keycloak_access_token
from app.utils.certificate_utils import get_info_from_url
import hashlib
import json

from datetime import timezone

logger = logging.getLogger("uvicorn.error")


def generate_hash256(input:str):
    # Input data

    dt=get_current_data_time()

    json_data={
  "endpointUsed": "'http://150.241.10.50:9102/diddocument",
  "providerId": "bad443d1-48dd-4efe-986b-ef915cfe3294",
  "providerName": "test_company",
  "userToken": "user_token",
  "userId": "dc57d527-0964-4146-8c4b-e50a526759fc",
  "userName": "geonikolai",
  "createdAt": dt,
  "ipAddress": "10.60.75.52",
  "datasets": [
    {}
  ]
}
    json_data = json.dumps(json_data, sort_keys=True)
    # Create a SHA-256 hash object
    sha256_hash = hashlib.sha256()

    # Update the hash object with the input data
    sha256_hash.update(json_data.encode())

    # Get the hexadecimal representation of the hash
    hash_hex = sha256_hash.hexdigest()
    print("SHA-256 Hash:", hash_hex)
    return hash_hex



def get_current_data_time():

    dt = datetime.now(timezone.utc).replace(tzinfo=None)
    dt=dt.strftime('%Y-%m-%dT%H:%M:%SZ')
    return dt

def send_create_did_log(domain):
    """
       Send did creation log to logging tool


       :return: true/false if the log has been sent
       :rtype: bool

    """
    dt=get_current_data_time()
    payload = {
            "endpointUsed": os.getenv("ONBOARDING_ENDPOINT")+"diddocument",
            "createdAt": dt,
            "ipAddress": "10.60.75.52",
            "domain":domain,
            "action:":"createDid"
        }
    str_payload=json.dumps(payload)
    return send_post_request(str_payload,os.getenv("LOGGER_DID_URL"))




def send_create_participant_log(domain, participantURL,legalName):
    """
       Send participant creation log to logging tool


       :return: true/false if the log has been sent
       :rtype: bool

    """
    dt=get_current_data_time()
    payload = {
            "endpointUsed": os.getenv("ONBOARDING_ENDPOINT")+"legalparticipant",
            "domain": domain,
            "participantURL":participantURL,
            "legalName":legalName,
            "createdAt": dt,
            "ipAddress": "10.60.75.52",
            "action:":"createParticipant"
    }
    str_payload = json.dumps(payload)
    return send_post_request(str_payload, os.getenv("LOGGER_PARTICIPANT_URL"))



def send_export_gaiax_model_log(dataproduct_internal_id):
    """
       Send a logs if the internal data product has been converted to Gaia-X correctly
       :return: true/false if the log has been sent
       :rtype: bool

    """



    dt=get_current_data_time()
    payload = {
        "endpointUsed": os.getenv("ONBOARDING_ENDPOINT")+"exportDataProductCompositionToGaiax",
         "internalDataProduct_id": dataproduct_internal_id,
        "createdAt": dt,
        "ipAddress": "10.60.75.52",
        "action": "exportGaiaxModel"
    }

    str_payload = json.dumps(payload)
    return send_post_request(str_payload, os.getenv("LOGGER_EXPORT_GAIAX_URL"))


def send_publish_data_product_log(domain, participantURL,dataproductName,openAPIURL):
    """
       Send a log if the dataproduct has been published correctly in Gaia-X ecosystem
       :return: true/false if the log has been sent
       :rtype: bool

    """

    legalName=get_legal_name(participantURL)


    dt=get_current_data_time()
    payload = {
        "endpointUsed": os.getenv("ONBOARDING_ENDPOINT") + "publish-dataproduct",
        "domain": domain,
        "participantURL": participantURL,
        "legalName": legalName,
        "dataProductName": dataproductName,
        "openAPIURL": openAPIURL,
        "createdAt": dt,
        "ipAddress": "10.60.75.52",
        "action:": "publishDataProduct"
    }

    str_payload = json.dumps(payload)
    return send_post_request(str_payload, os.getenv("LOGGER_PUBLISH_GAIAX_URL"))



def send_post_request(payload:str, url):


    # headers
    access_token = get_keycloak_access_token()
    headers = {'content-type': "application/json",
               'authToken':access_token,
               'accept': "*/*"

               }



    # Send the POST request
    try:
        response = requests.post(url, data=payload, headers=headers)
        if response.status_code == 200:
            return json.loads(response.text)
        else:
            print("Failed to send post request: {response.status_code}")
            return response.text
    except Exception as error:
        logger.debug("Error getting token from Keycloak: " + str(error))
    # Check the response


def get_legal_name(participantURL ):
    global legalName
    try:
        response = get_info_from_url(participantURL)

        if response is not None:
            participantSD = json.loads(response)
            print(type(participantSD))
            for elem in participantSD['verifiableCredential']:
                if elem['credentialSubject']['type'] == "gx:LegalParticipant":
                    legalName = elem['credentialSubject']['gx:legalName']
                    print(legalName)


    except Exception as error:
        logger.debug("Error getting legal name")
        legalName = ""


    return legalName