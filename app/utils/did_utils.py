"""
MIT License

Copyright (c) 2024 Tecnalia, Basque Research & Technology Alliance (BRTA)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""



"""This module give the functionality to work with did documents.
"""

import json
import logging
import os
from base64 import urlsafe_b64encode
from hashlib import sha1

from cryptography.hazmat.primitives import serialization
from app.models.didInputModel import DidInputModel
from app.models.didModel import PublicKeyJwk, VerificationMethodItem, DidModel
from app.utils.certificate_utils import generate_public_jwk
from app.utils.certificate_utils import locate_certificate


from app.utils.logger_utils import send_create_did_log

logger = logging.getLogger("uvicorn.error")






def generate_did(body: DidInputModel):


    """
       Generate a did document

       :param domain: domain
       :return: return a did document
       :rtype: str
    """


    print (body.domain)

    if len(body.didList) > 0:

        # fill context items array
        cxt = ["https://www.w3.org/ns/did/v1", "https://w3id.org/security/suites/jws-2020/v1"]
        didId = os.getenv("DID") + str(body.domain)
        # fill did Model using context, didId, verificationMethodList and assertion_method_list


        logger.debug("generating did Model")

        logger.debug("body.didList>0")
        logger.debug(f"body.didiList={body.didList}")
        assertion_method_list=[]
        verification_methodlist=[]
        index = 0
        for didElement in body.didList:

            global vm, publicKeyJwkJson, thumbprint, did
            logger.debug("generateDid::")

            logger.debug('domain={domain}')

            #locate certificate

            logger.debug("locate_certificate init")
            cert = locate_certificate(didElement.certificateURL)



            logger.debug("locate_certificate end")

            if cert is not None:



                logger.debug('didId={didId}')

                #generate PublicJWK from cert
                logger.debug("generating PublicJWK")

                publicKeyJwk = generate_public_jwk(cert)
                if publicKeyJwk is not None:
                    try:
                        thumbprint = sha1(cert.public_bytes(serialization.Encoding.DER))
                        publicKeyJwkJson = json.loads(publicKeyJwk.export())
                    except Exception as error:
                        logger.debug("Error generating publicKey in json format %s: " %error)

                    logger.debug("Adding x5u, x5t and alg to publicKeyJwk")
                    publicKeyJwkJson["x5u"]= didElement.certificateURL
                    publicKeyJwkJson["x5t"] = urlsafe_b64encode(thumbprint.digest()).rstrip(b'=').decode('utf-8')
                    publicKeyJwkJson["alg"] = didElement.algorithm

                    pk=PublicKeyJwk(e=publicKeyJwkJson.get("e"),kid=publicKeyJwkJson.get("kid"),kty=publicKeyJwkJson.get("kty"),n=publicKeyJwkJson.get("n"), x5u=publicKeyJwkJson.get("x5u"), x5t=publicKeyJwkJson.get("x5t"),alg=publicKeyJwkJson.get("alg"))

                    #create Verification Item using didId, and publicKey information

                    logger.debug("generating verification method item")
                    try:
                        vm = VerificationMethodItem(id=didId +"#"+str(didElement.verificationMethod),
                                                    type=str(didElement.keyType),
                                                    controller=didId, publicKeyJwk=pk)
                        # fill assertionMethod list
                        logger.debug("filling assertion method list")
                        assertion_method_list.append(didId + "#"+str(didElement.verificationMethod))
                        # fill verificationMethod list
                        verification_methodlist.append(vm)
                        logger.debug("verification method append")

                    except Exception as error:
                        logger.debug("Error generating verification method %s " % error)


            else:
                return "ERROR: There is a problem in did document generation process"


        try:
            did = DidModel(context=cxt, id=didId, verificationMethod=verification_methodlist,
                           assertionMethod=assertion_method_list)
        except Exception as error:
            logger.debug("Error generating did fro model: %s " % error)

        # create did.json file cform didModel
        logger.debug("saving did model into a did.json")
        fileNamePath = os.getenv("GAIAX_SD_FOLDER") + str("did.json")
        with open(fileNamePath, "w") as file:
            json.dump(did.model_dump(by_alias=True), file)

        # return did as json
        logger.debug("generated did.json content::%s" % did.model_dump(by_alias=True))

        # Call to LoggingTool

        try:
            send_create_did_log(domain)
            logger.debug("Sending log about did generation to Logger")
        except Exception as error:
            logger.debug("Error sending log about did creation to Logger%s" % error)

        return did.model_dump(by_alias=True)
    else:
        return "ERROR: There is a problem in did document generation process"




