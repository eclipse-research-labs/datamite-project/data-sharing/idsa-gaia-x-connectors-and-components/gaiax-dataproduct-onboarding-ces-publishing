<!--
  ~ Copyright (c) 2024 Tecnalia, Basque Research & Technology Alliance (BRTA)
  ~ 
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy of
  ~ this software and associated documentation files (the "Software"), to deal in
  ~ the Software without restriction, including without limitation the rights to
  ~ use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  ~ the Software, and to permit persons to whom the Software is furnished to do so,
  ~ subject to the following conditions:
  ~ 
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~ 
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  ~ FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  ~ COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  ~ IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  ~ CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  ~ 
  ~ SPDX-License-Identifier: MIT
-->

# DESCRIPTION

This project convert internal data products model into Gaia-X Data Products self-descriptions (SD).


![architecture_IDSA_GAIAX_Components](doc/figures/data-product-onboarding.png)

## Installation

1.- **Clone repository**
``` bash
git clone https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-sharing/idsa-gaia-x-connectors-and-components/gaiax-dataproduct-onboarding-ces-publishing.git
```
2.- **APACHE_DOCKER** directory

Place into this folder the correct contents for these files: 

- fullchain.pem
- privkey.pem

If the names are different from "fullchain.pem" and "privkey.pem", please enter into the folder and edit "Dockerfile" and place the name of the correct ones:

"**Dockerfile**" file

COPY ./fullchain.pem   /etc/ssl/certs/fullchain.pem

COPY ./privkey.pem  /etc/ssl/private/privkey.pem

2.1.- "**000-default.conf**" file

Change “Redirect” value for the correct one domain


2.2 "**default-ssl.conf**" file

Change “SSLCertificateFile” and “SSLCertificateKeyFile” only if you change the name for "fullchain.pem" and "privkey.pem" files


3.- Modify the value for some properties at "**datamite.env**" file.

**GAIA-X Service API URLs**:

- If the certificate is a “**Letsencript**” type:
  -	**NOTARIZATION_API**=https://registrationnumber.notary.lab.gaia-x.eu/development/registration-numbers/
  -	**COMPLIANCE_API**=https://compliance.lab.gaia-x.eu/development/api/credential-offers
  -	**CREDENTIALS_EVENT_SERVICE_API**=https://ces-development.lab.gaia-x.eu/credentials-events
- If the certificate is **not** a “**Letsencript**” type:
  -	**NOTARIZATION_API**=https://registrationnumber.notary.lab.gaia-x.eu/v1/registrationNumberVC
  -	**COMPLIANCE_API**=https://compliance.lab.gaia-x.eu/v1/api/credential-offers
  -	**CREDENTIALS_EVENT_SERVICE_API**= https://ces-v1.lab.gaia-x.eu/credentials-events


**CERTIFICATE_URL** property:

In this property you should be placed the URL for the certificate, with this format:

**CERTIFICATE_URL**=https://DOMAIN/.well-known/certificate.pem  

**PRIVATE_KEY** property:

**PRIVATE_KEY**="-----BEGIN PRIVATE KEY-----
MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCiQU6f0fVkZWmO
…….
fOssCXwbAgMBAAECggEAMNpyBmvss2xXcmZX3bvVjD05YPleWuY6eCIWMVYWRBYl
oaM3qfBpOaTtGqcFJn3Hc649i+G04B+xIGBqSKBkKArQtSs5zUyKnHLsJqtDqC1F
RoEnkJmWC7BV15CFdyBtng==
-----END PRIVATE KEY-----"

4.- For the the neo4j and apacheDocker services of the docker-compose file.

It can be seen some volumes of those services:

4.1 "apache"

Create a directory "apacheDir" behind of $HOME of the VM.

  - $HOME/apacheDir

4.2 "neo4j"

Create a directory "neo4j behind of $HOME of the VM.

- $HOME/neo4j


5.- UP all the services

``` bash
docker-compose up 
```


## Usage
To be defined.

## Roadmap
- [X] API Definition (Swagger).
- [X] Participant, DataProduct with Resources Gaia-X model
- [ ] Recover Participant information from internal Catalog (1st version)
- [ ] Recover DataProduct information from internal Catalog (1st version)
- [X] Develop "generateParticipant" Gaia-X method (1st version)
- [X] Develop "generateDataProduct with Resources" Gaia-X method (1st version)
- [ ] Integrate Create Participant with Frontend (1st version)
- [ ] Integrate generate Data Product with Resources with Frontend (1st version)
- [ ] Load data Product and participant Gaia-X self-descriptor (1st version)

## LICENSE
MIT


