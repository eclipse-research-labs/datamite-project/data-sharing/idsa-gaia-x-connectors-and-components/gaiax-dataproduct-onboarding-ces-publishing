Usage
=====

.. _installation:
Installation
------------

1. **Clone repository**

   .. code-block:: bash

      git clone https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-sharing/idsa-gaia-x-connectors-and-components/gaiax-dataproduct-onboarding-ces-publishing.git

2. **APACHE_DOCKER** directory

   Place into this folder the correct contents for these files:

   - fullchain.pem
   - privkey.pem

   If the names are different from "fullchain.pem" and "privkey.pem", please enter into the folder and edit "Dockerfile" and place the name of the correct ones:

   **Dockerfile** file

   .. code-block:: dockerfile

      COPY ./fullchain.pem   /etc/ssl/certs/fullchain.pem
      COPY ./privkey.pem  /etc/ssl/private/privkey.pem

   2.1. **000-default.conf** file

        Change “Redirect” value for the correct one domain

   2.2. **default-ssl.conf** file

        Change “SSLCertificateFile” and “SSLCertificateKeyFile” only if you change the name for "fullchain.pem" and "privkey.pem" files

3. Modify the value for some properties at **datamite.env** file.

   **GAIA-X Service API URLs**:

   - If the certificate is a **Letsencript** type:
     - **NOTARIZATION_API**=https://registrationnumber.notary.lab.gaia-x.eu/development/registration-numbers/
     - **COMPLIANCE_API**=https://compliance.lab.gaia-x.eu/development/api/credential-offers
     - **CREDENTIALS_EVENT_SERVICE_API**=https://ces-development.lab.gaia-x.eu/credentials-events
   - If the certificate is **not** a **Letsencript** type:
     - **NOTARIZATION_API**=https://registrationnumber.notary.lab.gaia-x.eu/v1/registrationNumberVC
     - **COMPLIANCE_API**=https://compliance.lab.gaia-x.eu/v1/api/credential-offers
     - **CREDENTIALS_EVENT_SERVICE_API**= https://ces-v1.lab.gaia-x.eu/credentials-events

   **CERTIFICATE_URL** property:

   In this property you should be placed the URL for the certificate, with this format:

   **CERTIFICATE_URL**=https://DOMAIN/.well-known/certificate.pem

   **PRIVATE_KEY** property:

   .. code-block:: text

      **PRIVATE_KEY**="-----BEGIN PRIVATE KEY-----
      MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCiQU6f0fVkZWmO
      …….
      fOssCXwbAgMBAAECggEAMNpyBmvss2xXcmZX3bvVjD05YPleWuY6eCIWMVYWRBYl
      oaM3qfBpOaTtGqcFJn3Hc649i+G04B+xIGBqSKBkKArQtSs5zUyKnHLsJqtDqC1F
      RoEnkJmWC7BV15CFdyBtng==
      -----END PRIVATE KEY-----"

4. For the the neo4j and apacheDocker services of the docker-compose file.

   It can be seen some volumes of those services:

   4.1 "apache"

   Create a directory "apacheDir" behind of $HOME of the VM.

   - $HOME/apacheDir

   4.2 "neo4j"

   Create a directory "neo4j" behind of $HOME of the VM.

   - $HOME/neo4j

5. UP all the services

   .. code-block:: bash

      docker-compose up


